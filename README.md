# Podcast Kujua

This repository contains accompanying code and documents for the Kujua Podcast.

**What is the podcast about?**

The format of the podcasts is a bit different from other podcasts. It is more like an audible article and takes between 15 and 45 minutes to listen to.

_Reviews_ are talks about software or hardware for developers.

_Training sessions_ are about one development topic.

Notes and other relevant documents for all podcasts can be found in this repository.
